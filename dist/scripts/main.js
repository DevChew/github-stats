class RepoSwitcher {

  constructor(repositoriesArray){
    this.repos = repositoriesArray;
    this.currentRepo = '';
    this.build_main_menu(repositoriesArray);

    if (window.location.hash !== ""){
      this.change_repo(window.location.hash.substr(1));
    } else {
      this.change_repo(repositoriesArray[0].slug);
    }

  }

  get_repo_panels_data(user,name){
    var gh = new GitHub();
    const thisC = this;
    if (
      typeof user == 'undefined' ||
      user == '' ||
      typeof name == 'undefined' ||
      name == ''
    ){ return false };

    var repo = gh.getRepo(user,name);
    thisC.set_load('on');
    repo
      .getDetails()
      .then(function(rep) {
        thisC.set_panels_data(rep.data);
        //console.log(rep.data);
        thisC.set_load('off');
      });
  }

  set_load(state){
    if (
      state == 'off'
    ) {
      document.querySelector('body').classList.remove('loading');
    } else {
      document.querySelector('body').classList.add('loading');
    }
  }

  set_repo_panel_data(panel,repoData){
    if (
      typeof panel == 'undefined' ||
      panel == '' ||
      typeof repoData == 'undefined' ||
      repoData.length
    ){ return false };

    const thisPanel = document.querySelector('[data-panel='+panel+']');
    if (thisPanel == null) return false;

    const dataName = thisPanel.getAttribute('data-panel-data');
    if (dataName == null) return false;
    if (repoData[dataName] == null) return false;

    thisPanel.querySelector('.data').innerHTML = repoData[dataName];
  }

  set_panels_data(data){
    const thisC = this;
    if (
      typeof data == 'undefined' ||
      data.length
    ){ return false };

    const panels = document.querySelectorAll('[data-panel]');
    panels.forEach(function(element) {
      thisC.set_repo_panel_data(element.getAttribute('data-panel'), data);
    });
  }

  build_main_menu(repos){
    const thisC = this;
    if (
      typeof repos == 'undefined'
    ){ return false };

    document.querySelector('.main-nav ul').innerHTML = repos.map( function(item){
      return '<li><a href="#'+item.slug+'">'+item.name+'</a></li>';
    }).join('');

    document.querySelectorAll('.main-nav a').forEach(link => link.addEventListener('click', function(e){
      thisC.change_repo(this.hash.substr(1));
    }));
  }

  change_repo(repo_slug){
    const thisRepo = this.repos.filter(repo => (repo.slug == repo_slug));
    if( thisRepo.length ){
      if (this.currentRepo !== thisRepo[0].slug){
        this.get_repo_panels_data(thisRepo[0].user, thisRepo[0].name);
        this.currentRepo = thisRepo[0].slug;
        console.log('repo change to ' + this.currentRepo );
      }
    }
  }
}

const repSw = new RepoSwitcher([
  {
    slug: 'angular',
    name: 'angular',
    user: 'angular',
  },
  {
    slug: 'react',
    name: 'react',
    user: 'facebook',
  },
  {
    slug: 'laravel',
    name: 'laravel',
    user: 'laravel',
  },
  {
    slug: 'bootstrap',
    name: 'bootstrap',
    user: 'twbs',
  },
  {
    slug: 'atom',
    name: 'atom',
    user: 'atom',
  },
]);
